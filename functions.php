<?php

// Defines
define( 'WEAREWP_THEME_DIR', get_stylesheet_directory() );
define( 'WEAREWP_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );


//	interpretation des shortcodes dans le widget text
if ( !is_admin() ) {
    add_filter('widget_text', 'do_shortcode', 11);
}

add_action( 'after_setup_theme', '_wearewp_setup' );
function _wearewp_setup() {
	global $cap, $content_width;
	
	//	Languages
	load_theme_textdomain( 'wearewp', get_stylesheet_directory() . '/languages' );


	add_filter('widget_text', 'do_shortcode');
	
	remove_action( 'wp_head', 'feed_links_extra', 3 ); 				// Display the links to the extra feeds such as category feeds
	remove_action( 'wp_head', 'feed_links', 2 );					// Display the links to the general feeds: Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' );							// Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' );					// Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'index_rel_link' );					// index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );		// prev link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );		// start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );	// Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'wp_generator' );						// Display the XHTML generator that is generated on the wp_head hook, WP version
	
	remove_action( 'wp_head', 'rest_output_link_wp_head' );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	
	
	// Menus
	
}



//	Suppression des titres h dans les widgets
add_action( 'widgets_init', '_wearewp_unregister_sidebar', 11 );
function _wearewp_unregister_sidebar() {
	unregister_sidebar('blog-sidebar');
	unregister_sidebar('footer-col');
	unregister_sidebar('footer-col-1');
	unregister_sidebar('footer-col-2');
	unregister_sidebar('footer-col-3');
	unregister_sidebar('footer-col-4');
}

add_action( 'widgets_init', '_wearewp_register_sidebar', 12 );
function _wearewp_register_sidebar() {
	//	Sidebars
	register_sidebar(array(
		'name'          => __('Primary Sidebar', 'fl-automator'),
		'id'            => 'blog-sidebar',
		'before_widget' => '<aside id="%1$s" class="fl-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="fl-widget-title">',
		'after_title'   => '</div>'
	));
	
	$footer_widgets_display = FLTheme::get_setting('fl-footer-widgets-display');
	
	// Footer Widgets
	if ( $footer_widgets_display != 'disabled' ) {
		register_sidebars( 4, array(
			'name'          => _x( 'Footer Column %d', 'Sidebar title. %d stands for the order number of the auto-created sidebar, 4 in total.', 'fl-automator' ),
			'id'            => 'footer-col',
			'before_widget' => '<aside id="%1$s" class="fl-widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<div class="fl-widget-title">',
			'after_title'   => '</div>'
		) );
	}
}



//	supprime la class css wp-image
add_filter( 'get_image_tag_class', '_wearewp_get_image_tag_class_filter', 10, 4 );
function _wearewp_get_image_tag_class_filter( $class, $id, $align, $size ) {
	$class = str_replace( 'wp-image-'.$id, '', $class );
	
	return $class;
}

//	Add html5 figure to video
add_filter('embed_oembed_html', '_wearewp_oembed_filter', 10, 4 ) ;
function _wearewp_oembed_filter($html, $url, $attr, $post_ID) {
	$return = '<figure class="video-container">'. $html .'</figure>';
	
	return $return;
}

/** Gravity Forms **/
// suppression des tabindex
add_filter( 'gform_tabindex', '__return_false' );


//	A11Y
add_action('fl_page_open', '_wearewp_a11y_goto_content');
function _wearewp_a11y_goto_content() {
	echo '<a href="#fl-page-content" class="sr-only sr-only-focusable">'. _x('Go to content', 'a11y', 'wearewp') .'</a>';
}
